#!/usr/bin/env python
#coding=utf-8

import numpy as np
import matplotlib.pylab as plt
import time


####################################################################
############ Functions for the spiking dynamics ###########
####################################################################

def generate_conductance_shotnoise(freq, t, N, Q, Tsyn, g0=0, seed=0):
    """
    generates a shotnoise convoluted with a waveform
    frequency of the shotnoise is freq,
    K is the number of synapses that multiplies freq
    g0 is the starting value of the shotnoise
    """
    if freq==0:
        # print "problem, 0 frequency !!! ---> freq=1e-9 !!"
        freq=1e-9
    upper_number_of_events = max([int(3*freq*t[-1]*N),1]) # at least 1 event
    np.random.seed(seed=seed)
    spike_events = np.cumsum(np.random.exponential(1./(N*freq),\
                             upper_number_of_events))
    g = np.ones(t.size)*g0 # init to first value
    dt, t = t[1]-t[0], t-t[0] # we need to have t starting at 0
    # stupid implementation of a shotnoise
    event = 0 # index for the spiking events
    for i in range(1,t.size):
        g[i] = g[i-1]*np.exp(-dt/Tsyn)
        while spike_events[event]<=t[i]:
            g[i]+=Q
            event+=1
    return g

def generate_up_and_down_conductance_shotnoise(down_freq, up_freq, t, Q_down, Q_up,\
                    Tsyn, seed=0, T1=.5, T2=1.25, T3=1.75):
    """
    generates a shotnoise convoluted with a waveform
    frequency of the shotnoise is freq,
    g0 is the starting value of the shotnoise

    here alternation of DOWN, UP, DOWN, UP
    """
    dt, t = t[1]-t[0], t-t[0] # security for t
    it1, it2, it3 = int(T1/dt), int(T2/dt), int(T3/dt)
    
    g0 = generate_conductance_shotnoise(down_freq, t[:it1], 1,\
                    Q_down, Tsyn, g0=0, seed=seed)

    g1 = generate_conductance_shotnoise(up_freq, t[it1:it2], 1,\
                    Q_up, Tsyn, g0=g0[-1], seed=seed+1)
    g2 = generate_conductance_shotnoise(down_freq, t[it2:it3], 1,\
                    Q_down, Tsyn, g0=g1[-1], seed=seed+2)
    g3 = generate_conductance_shotnoise(up_freq, t[it3:], 1,\
                    Q_up, Tsyn, g0=g2[-1], seed=seed+3)
    return np.concatenate([g0, g1, g2, g3])


def iaf_sim(t, ge, gi, I, cell_params, v0=0):
    """ functions that solve the membrane equations for the
    IAF model for 2 time varying excitatory and inhibitory
    conductances as well as a current input
    returns : v, spikes
    """
            
    ### parameters of the IAF model
    vthresh, vreset, vpeak = cell_params['Vthre'], cell_params['Vreset'], 5e-3
    trefrac = cell_params['Trefrac']
    El, Gl = cell_params['El'], cell_params['Gl']
    ### driving force parameters
    Ee, Ei, Cm = cell_params['Ee'], cell_params['Ei'], cell_params['Cm']
    
    last_spike = -np.inf # time of the last spike, for the refractory period
    spikes, potential_trace = [], []
    dt = t[1]-t[0]

    if v0==0: # the flag to say : no particular initial value
        v=El
    else:
        v=v0

    for i in range(len(t)):

        if (t[i]-last_spike)>trefrac: # only when non refractory
            ## Vm dynamics calculus
            v = v + dt/Cm*(I[i] + Gl*(El-v) + ge[i]*(Ee-v) + gi[i]*(Ei-v))
        if v > vthresh:
            if v==vpeak: # already spiking before
                v = vreset
            else:
                v = vpeak  # spiking now, aesthetics
                last_spike = t[i]
                spikes.append(last_spike)
        potential_trace.append(v)
    
    potential_trace = np.array(potential_trace)
    return potential_trace, spikes


def single_exp(t, fe, fi, params, seed=0):
    ge = generate_conductance_shotnoise(fe, t, params['Ke'], params['Qe'], params['Te'], g0=0, seed=seed)
    gi = generate_conductance_shotnoise(fi, t, params['Ki'], params['Qi'], params['Ti'], g0=0, seed=seed)
    v, spikes = iaf_sim(t, ge, gi, np.zeros(len(t)), params, v0=0)
    return v, spikes


def single_exp_up_down(params, seed=0, tstop=2.5, stim_up=None, stim_down=None):
    dt = 1e-5
    t =  np.arange(int(tstop/dt))*dt
    ge = generate_up_and_down_conductance_shotnoise(\
            params['fe_down'], params['fe_up'], t,\
            params['Qe_down'], params['Qe_up'], params['Te'], seed=seed)
    gi = generate_up_and_down_conductance_shotnoise(\
            params['fi_down'], params['fi_up'], t,\
            params['Qi_down'], params['Qi_up'], params['Ti'], seed=seed)

    # additional stimulation
    if stim_down is not None:
        [t0, Q0] = stim_down
        ge[int(t0/dt):] += Q0*np.exp(-(t[int(t0/dt):]-t0)/params['Te'])
    if stim_up is not None:
        [t0, Q0] = stim_up
        ge[int(t0/dt):] += Q0*np.exp(-(t[int(t0/dt):]-t0)/params['Te'])
    
    v, spikes = iaf_sim(t, ge, gi, np.zeros(len(t)), params, v0=0)
    return t, v, spikes

