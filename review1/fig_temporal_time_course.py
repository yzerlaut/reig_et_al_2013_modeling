import numpy as np
import matplotlib.pyplot as plt
import pickle


params = {\
#Neuron parameters
'El' : -70.e-3,
'Vthre' : -50.e-3,
'Vreset':-60e-3,
'Trefrac':5e-3,
'Cm': 200.e-12,
'Gl' : 10.e-9,
#Synaptic parameters
'Te' : 7.3e-3,
'Ti' : 5.e-3,
'Ee' : 0.,
'Ei' : -80.e-3,
#Network parameters
'Ke'  : 1.,
'Ki'  : 1.
}


mgi_up, sgi_up, mge_up, sge_up = 20*1e-9, 8*1e-9, 7*1e-9, 3*1e-9
# mgi_down, sgi_down, mge_down, sge_down = 2*1e-9, .5*1e-9, .3*1e-9, .1*1e-9

# for thalamic down state
mgi_down, sgi_down, mge_down, sge_down = 8*1e-9, 3*1e-9, 3.*1e-9, 2*1e-9

# up state parameters
params['fi_up'], params['fe_up'] = (mgi_up/sgi_up)**2/2/params['Ti'],\
   (mge_up/sge_up)**2/2/params['Te']
params['Qi_up'], params['Qe_up'] = 2*sgi_up**2/mgi_up, 2*sge_up**2/mge_up

# down state parameters
params['fi_down'], params['fe_down'] = (mgi_down/sgi_down)**2/2/params['Ti'],\
   (mge_down/sge_down)**2/2/params['Te']
params['Qi_down'], params['Qe_down'] = 2*sgi_down**2/mgi_down, 2*sge_down**2/mge_down


import simulations as sim

fig = plt.figure(figsize=(8,5))
ax1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
ax1.set_ylabel('$V_{m}$ (mV)')
Full_spikes = np.empty(0)

tstop = 3.

SEED = range(70)
for s in SEED:
    t, v, spikes = sim.single_exp_up_down(params, seed=s, tstop=tstop,\
                        stim_down=[1.5, 2.*params['Qe_up']],
                        stim_up=[2.5, 2.*params['Qe_up']])
    if s<5:
        ax1.plot(t, 1e3*v, alpha=.5, lw=.5)
    Full_spikes = np.concatenate([Full_spikes, spikes])


y, x = np.histogram(Full_spikes, bins=np.linspace(0,tstop,50,endpoint=True))

y, x = y/(x[1]-x[0])/len(SEED), x[:-1]
ax2 = plt.subplot2grid((3,1), (2,0))
ax2.bar(x, y, width=x[1]-x[0], color='w', lw=1, edgecolor='k', label='simulation')
ax2.set_xlim([0,tstop])
ax2.set_ylabel('$\\nu_{out}$ (Hz)')
ax2.set_xlabel('time (s)')
ax1.arrow(1.5, -20, 0.0, -20, fc="r", ec="r", head_width=0.05, head_length=3)
ax1.arrow(2.5, -20, 0.0, -20, fc="r", ec="r", head_width=0.05, head_length=3)

# finally analytic description

fig.savefig('fig_sup.png', format='png')
plt.tight_layout()
plt.show()

