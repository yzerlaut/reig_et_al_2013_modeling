import numpy as np
import matplotlib.pylab as plt

data = np.loadtxt('data.csv', delimiter=',')

psp_down, psp_norm = [], []

# reformating the data
IN = True
for vec in data.T:
    if IN:
        psp_down.append(vec[vec!=0])
        IN = False
    else:
        psp_norm.append(vec[vec!=0])
        IN = True

psp_norm_fit, psp_down_fit = [], []
# ordering        
for i in range(len(psp_down)):
    i_order = np.argsort(psp_down[i])
    psp_down[i] = psp_down[i][i_order]
    psp_norm[i] = psp_norm[i][i_order]

marker = ['^--', '*-', 'o:']
label = ['TC stim', 'IC stim', 'Aud stim']
alpha = [.8, .4, 1.]

def log_plot(subplot, psp_down_fit=None, psp_norm_fit=None, data=True):

    subplot.semilogx([1e-9,30],[1,1], 'k--', lw=.5)
    
    if psp_down_fit and psp_norm_fit:
        for i in [1,0,2]:
            subplot.semilogx(psp_down_fit[i], psp_norm_fit[i], marker[i],\
                color='k', ms=0, lw=6, alpha=alpha[i], label=label[i])
                         
    if data:
        for i in [1,0,2]:
            subplot.semilogx(psp_down[i], psp_norm[i], marker[i], color='k', ms=7,\
                     lw=.7, label=label[i])

    subplot.set_xlabel(r"$PSP_\max^{Down}$(mV)")
    plt.xticks([1,2,5,10,20], ['1','2','5','10','20'])
    subplot.set_xlim([.6,21])
    subplot.set_ylabel('modulation factor')
    subplot.set_ylim([.6,6.2])
    subplot.legend(frameon=False)
    # subplot.legend(frameon=False,prop={'size':'small'})


def plot(subplot, psp_down_fit=None, psp_norm_fit=None, data=True):

    if psp_down_fit and psp_norm_fit:
        for i in [1,0,2]:
            subplot.plot(psp_down_fit[i], psp_norm_fit[i], marker[i],\
                         color='k', ms=0, lw=6, alpha=alpha[i], label=label[i])

    if data:
        for i in [1,0,2]:
            subplot.plot(psp_down[i], psp_norm[i], marker[i], color='k', ms=7,\
                     lw=.5, label=label[i])

    subplot.set_xlabel(r"$PSP_\max^{Down}$(mV)")
    # subplot.set_xticks([1,2,5,10,20], ['1','2','5','10','20'])
    subplot.set_xlim([.1,18])
    subplot.set_ylabel('modulation factor')
    subplot.set_ylim([.6,6.2])
    subplot.plot([0,30],[1,1], 'k--', lw=.5)
    # subplot.legend(frameon=False,prop={'size':'small'})
    subplot.legend(frameon=False)

##############################################################################
### -------------------------- FITTING  ---------------------------------- ###
##############################################################################

from scipy.optimize import minimize

x0 = [1. , 1.]

def func(psp, P): # UNCOMMENT THE PARAMETER AND FUNCTION YOU WANT
    ########## two parameters functions
    [a, b] = P
    # y = a*psp+b # linear
    y = a*np.log(psp)+b # linear when we semilogx !!!
    # y = a*np.exp(-b*np.log(psp)) # exponetial when we semilogx !!!
    # y = a*np.exp(-b*psp) # exponential
    # y = a*np.exp(-b*psp**.1) # exponential of 10th root
    ########## three parameters functions
    # [a, b, c] = P
    # y = a*psp**2+b*psp+c # quadratic ----> not good !
    # y = a*(np.exp(-b*psp)+np.exp(-c*psp)) # exponential
    ########## four parameters functions
    # [a, b, c, d] = P
    # y = a*np.exp(-b*psp)+c*np.exp(-c*psp) # double exponential
    return y
    
for i in range(len(psp_down)):

    def to_minimize(x):
        return np.sum(((psp_norm[i]-\
            func(psp_down[i], x))/psp_norm[i])**2)\
            /len(psp_norm)**2
            
    res = minimize(to_minimize, x0)
    psp_down_fit.append(np.linspace(psp_down[i].min(),psp_down[i].max(),1e2))
    psp_norm_fit.append(func(psp_down_fit[i], res.x))
    print res

fig = plt.figure(figsize=(12, 9))
s1 = plt.subplot(221)    
plot(s1, psp_down_fit, psp_norm_fit)
s2 = plt.subplot(222)    
plot(s2, psp_down_fit, psp_norm_fit, data=False)
s3 = plt.subplot(223)    
log_plot(s3, psp_down_fit, psp_norm_fit)
s4 = plt.subplot(224)    
log_plot(s4, psp_down_fit, psp_norm_fit, data=False)

plt.tight_layout()
plt.show()



# ##############################################################################
# ### ---------------- DOUBLE Exponential ---------------------------------- ###
# ##############################################################################

# from scipy.optimize import minimize

# def double_exponential(psp, P):
#     [a, b, c, d] = P
#     y = a*np.exp(-psp*b)+c*np.exp(-psp*d)
#     return y
    
# for i in range(len(psp_down)):

#     def to_minimize(x):
#         return np.sum((psp_norm[i]-\
#             double_exponential(psp_down[i], x))**2)\
#             /len(psp_norm)**2
            
#     x0 = [1. , 1., 1., 1.]
#     res = minimize(to_minimize, x0)
#     # linear fit of the log
#     psp_down_fit.append(np.linspace(psp_down[i].min(),psp_down[i].max(),1e2))
#     psp_norm_fit.append(double_exponential(psp_down_fit[i], res.x))

# fig = plt.figure(figsize=(12, 9))
# s1 = plt.subplot(221)    
# log_plot(s1, psp_down_fit, psp_norm_fit)
# s2 = plt.subplot(222)    
# log_plot(s2, psp_down_fit, psp_norm_fit, data=False)
# s3 = plt.subplot(223)    
# fig = plot(s3, psp_down_fit, psp_norm_fit)
# s4 = plt.subplot(224)    
# plot(s4, psp_down_fit, psp_norm_fit, data=False)

# plt.tight_layout()
# plt.show()
    
