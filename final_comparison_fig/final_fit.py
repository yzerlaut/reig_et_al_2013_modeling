import numpy as np
import matplotlib.pylab as plt

data = np.loadtxt('data.csv', delimiter=',')

psp_down, psp_norm = [], []

# reformating the data
IN = True
for vec in data.T:
    if IN:
        psp_down.append(vec[vec!=0])
        IN = False
    else:
        psp_norm.append(vec[vec!=0])
        IN = True

psp_norm_fit, psp_down_fit = [], []
# ordering        
for i in range(len(psp_down)):
    i_order = np.argsort(psp_down[i])
    psp_down[i] = psp_down[i][i_order]
    psp_norm[i] = psp_norm[i][i_order]

marker = ['^--', '*-', 'o:']
label = ['TC stim', 'IC stim', 'Aud stim']
alpha = [.8, .4, 1.]

def log_plot(subplot, psp_down_fit=None, psp_norm_fit=None, data=True):

    if psp_down_fit and psp_norm_fit:
        for i in [1,0,2]:
            subplot.semilogx(psp_down_fit[i], psp_norm_fit[i], marker[i],\
                color='k', ms=0, lw=4, alpha=alpha[i], label=label[i])
                         
    if data:
        for i in [1,0,2]:
            subplot.semilogx(psp_down[i], psp_norm[i], marker[i], color='k', ms=7,\
                     lw=.7, label=label[i])

    subplot.semilogx([1e-9,30],[1,1],'k--',lw=1)
    subplot.set_xlabel(r"$PSP_\max^{Down}$(mV)")
    plt.xticks([1,2,5,10,20], ['1','2','5','10','20'])
    subplot.set_xlim([.6,21])
    subplot.set_ylabel('modulation factor')
    subplot.set_ylim([.6,6.2])
    subplot.legend(frameon=False)
    # subplot.legend(frameon=False,prop={'size':'small'})


def plot(subplot, psp_down_fit=None, psp_norm_fit=None, data=True):

    if psp_down_fit and psp_norm_fit:
        for i in [1,0,2]:
            subplot.plot(psp_down_fit[i], psp_norm_fit[i], marker[i],\
                         color='k', ms=0, lw=3, alpha=alpha[i], label=label[i])

    if data:
        for i in [1,0,2]:
            subplot.plot(psp_down[i], psp_norm[i], marker[i], color='k', ms=7,\
                     lw=.5, label=label[i])

    subplot.plot([0,30],[1,1],'k--',lw=1)
    subplot.set_xlabel(r"$PSP_\max^{Down}$(mV)")
    # subplot.set_xticks([1,2,5,10,20], ['1','2','5','10','20'])
    subplot.set_xlim([.1,18])
    subplot.set_ylabel('modulation factor')
    subplot.set_ylim([.6,6.2])
    # subplot.legend(frameon=False,prop={'size':'small'})
    subplot.legend(frameon=False)

##############################################################################
### -------------------------- FITTING  ---------------------------------- ###
##############################################################################

from scipy.optimize import minimize

# exponential fit for the first IC and TC

x0 = [1. , 1.]

def func(psp, P): # UNCOMMENT THE PARAMETER AND FUNCTION YOU WANT
    ########## two parameters functions
    [a, b] = P
    y = a*np.exp(-b*psp) # exponential
    # y = a*np.exp(-b*np.log(psp)) # exponential when we semilogx !!!
    return y
    
for i in range(len(psp_down)-1):

    def to_minimize(x):
        return np.sum((psp_norm[i]-\
            func(psp_down[i], x))**2)\
            /len(psp_norm)**2
            
    res = minimize(to_minimize, x0)
    psp_down_fit.append(np.linspace(psp_down[i].min(),psp_down[i].max(),1e2))
    psp_norm_fit.append(func(psp_down_fit[i], res.x))
    print res

x0 = [1. , 1., .1, 1.]

def func(psp, P): # UNCOMMENT THE PARAMETER AND FUNCTION YOU WANT
    [a, b, c, d] = P
    y = a*(np.exp(-b*psp))+d*(np.exp(-c*psp)) # exponential
    return y
    
for i in range(2,len(psp_down)):
    def to_minimize(x):
        return np.sum((psp_norm[i]-\
            func(psp_down[i], x))**2)\
            /len(psp_norm)**2
            
    res = minimize(to_minimize, x0)
    psp_down_fit.append(np.linspace(psp_down[i].min(),psp_down[i].max(),1e2))
    psp_norm_fit.append(func(psp_down_fit[i], res.x))
    print res

fig = plt.figure(figsize=(6,4))
s1 = plt.subplot(111)    
log_plot(s1, psp_down_fit, psp_norm_fit, data=True)

plt.tight_layout()
plt.show()
# fig.savefig('/home/yann/Desktop/exp_for_IC_TC--double_exp_for_aud.svg', format='svg')
