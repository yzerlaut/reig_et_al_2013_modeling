import numpy as np
import matplotlib.pyplot as plt

"""
we load the data, 0 have been introduced when no measures
have been taken, the "--" sign, this will need to be handled
carefully
raw are mean, variance, mean, variance
"""
data_up = np.loadtxt('auditory_up.csv', delimiter=',')
data_down = np.loadtxt('auditory_down.csv', delimiter=',')

Istim = data_up[0] # first line is stimulation

plt.figure(figsize=(10,7))
for ii in range(1, len(data_up)): # loop over cells
    ##we delete the zeros from the data
    up, down, i_up, i_down, s_down, s_up = [], [], [], [], [], []
    for jj in range(int(len(Istim)/2.)-1):
        if data_up[ii][2*jj+1]!=0:
            up.append(data_up[ii][2*jj])
            s_up.append(data_up[ii][2*jj+1])
            i_up.append(Istim[2*jj])
        if data_down[ii][2*jj+1]!=0:
            down.append(data_down[ii][2*jj])
            s_down.append(data_down[ii][2*jj+1])
            i_down.append(Istim[2*jj])
    plt.subplot2grid((4,3), ((ii-1)%4, int((ii-1)/4.)))
    plt.errorbar(i_up, up, s_up,  marker='D', color='k', lw=2, label='Up')
    plt.errorbar(i_down, down, s_down, marker='D',  color='gray',\
                 lw=2, label='Down')
    plt.title('cell '+str(ii))
    # plt.ylim([0.1,30])
    plt.xlim([50, 90])
    plt.locator_params(nbins=4)
plt.tight_layout()
plt.savefig('auditory_individual.svg')


## then we construct the mean graph
rate_mean, rate_std = [], [] # for normalized PSP
up_mean, up_std, down_mean, down_std = [], [], [], [] # for absolute PSP
for jj in range(len(Istim[1::2])): # loop over levels
    raw_rate, raw_up, raw_down = [], [], []
    for ii in range(1, len(data_up)): # loop over cells
        if data_down[ii][2*jj+1]!=0 and data_up[ii][2*jj+1]!=0:
            raw_rate.append(data_up[ii][2*jj]/data_down[ii][2*jj])
            raw_up.append(data_up[ii][2*jj])
            raw_down.append(data_down[ii][2*jj])
    rate_mean.append(np.array(raw_rate).mean())
    rate_std.append(np.array(raw_rate).std())
    up_mean.append(np.array(raw_up).mean())
    up_std.append(np.array(raw_up).std())
    down_mean.append(np.array(raw_down).mean())
    down_std.append(np.array(raw_down).std())


plt.figure(figsize=(6,4))
plt.errorbar(Istim[1::2], down_mean, down_std, marker='D', color='gray',\
             lw=3, label='Down state')
plt.errorbar(Istim[1::2], up_mean, up_std, marker='D', color='k', lw=3, label='Up state')
plt.yscale('log')
plt.yticks([0.1, 0.5, 1, 5, 10],['0.1', '0.5', '1', '5', '10'])
#plt.legend(loc='lower right')
plt.title('absolute PSP')
plt.ylabel(r"max($\Delta V(t)$)")
plt.xlabel(r"Stim (dB)")
plt.xlim([50, 90])
plt.ylim([0.7,20])
plt.tight_layout()
plt.savefig('auditory_absolute.svg')

    
plt.figure(figsize=(6,4))
plt.plot(Istim[1::2], np.ones(len(rate_mean)), color='gray', lw=5,\
         label='Down state')
plt.errorbar(Istim[1::2], rate_mean, rate_std, color='k', lw=3, label='Up state')
plt.legend()
plt.title('normalized PSP')
plt.ylabel(r"max($\Delta V(t)$)")
plt.xlabel(r"Stim (dB)")
plt.xlim([50, 90])
plt.tight_layout()
plt.savefig('auditory_normalized.svg')

from scipy import interpolate
# tck = interpolate.splrep(np.array(down_mean), np.array(rate_mean))
# xnew = np.linspace(down_mean.min(),down_mean.max())
# ynew = interpolate.splev(xnew,tck,der=1)
print np.array(down_mean)
print np.array(rate_mean)

plt.figure(figsize=(6,4))
plt.plot(down_mean, np.ones(len(rate_mean)), color='gray', lw=5)
# plt.plot(xnew, ynew, color='gray', lw=5)
plt.plot(down_mean, rate_mean, color='k', lw=3, label='Auditory stim')
plt.legend()
plt.xlabel('PSP$_{max}$ $^{Down}$')
plt.ylabel(r"modulation factor")
plt.tight_layout()
plt.savefig('auditory_modulation_factor.svg')




