\documentclass[11pt,a4paper,twocolumn]{article}

\usepackage{graphicx}
\usepackage[fleqn]{amsmath}
\usepackage{amssymb,mathenv,array}
\usepackage[colorlinks=true]{hyperref}
\hypersetup{allcolors = blue} % to have all the hyperlinks in 1 color
\usepackage[textwidth=18cm,columnsep=1cm,bottom=1.4cm,top=1.5cm]{geometry}


\begin{document}

\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{fig1.pdf}
  \caption{Network excitability and cell properties differences during
    Up and Down states explain the gain modulation of the post
    synaptic potentials as a response to a stimulation within the
    cortical network. \textbf{A} the bipolar stimulation creates a
    space-dependent extracellular current, \textbf{B} this results in
    an heterogenous stimulation within the local cortical network
    given the decaying stimulus intensity $I(r)$ and the increasing
    cell density $N(r)$. \textbf{C} The activation function (Eq xx)
    represents the excitability of the local cortical
    network. \textbf{D} \& \textbf{E} The number of responsive neurons
    to the stimulation depends on the activation function and of the
    stimulation level, example is shown for $I_0=80 \mu$A for Up and
    Down states respectively, the activation curves have been adapted
    to the stimulus thanks to the rule XX. \textbf{F} For each
    stimulation value we can then calculate the number of neurons
    activated by the stimulation. \textbf{G} For given recurrence
    parameters ($\epsilon=5\%$ and $g=0.25$), we can estimate the
    network input to the recorded cell and deduce the post-synaptic
    potential from formula XX, this should be compared to the
    experimental results of Figure 1.}
  \label{fig:main}
\end{figure*}
 

\section{Methods}
\label{sec:model}

\subsection{Cell model}
\label{sec:cell-model}

We model the neuron as a single compartment, i.e. we neglect the
spatial extent of the neurons and we consider that the whole
computation can be described by a single value: the membrane potential
of the soma. We adapt synaptic parameters to reproduce somatic
response (in particular the excitatory one). \\

In the subthreshold regime neuron will only exhibit 2 features:
passive and synaptic currents.

The passive properties are described by a simple RC circuit, the
capacitive current is characterized by a membrane capacitance $C_m$
the leak current is set by a conductance $g_L$ and a reversal
potential $E_L$.

\begin{equation}
  \label{eq:single-comp}
  C_m \frac{dV}{dt} = g_L \, \big(E_L - V\big) + I^{syn}(V,t)
\end{equation}

The synaptic currents $I^{syn}(V,t)$ is made of excitatory and
inhibitory input with reversal potentials $E_e$ and $E_i$. Their
respective conductances $G_e$ and $G_i$ will be determined by the sum
of the background and stimulus evoked activity.

\begin{equation}
  \label{eq:syn-current}
  I^{syn}(V,t) = G_e(t) \, \big(E_e - V\big) + G_i(t) \, \big(E_i - V\big)
\end{equation}


\subsection{Cortical input : background activity}
\label{sec:ntwk-background}

The cortical network sends excitatory and inhibitory input to the
recorded cell, we will model this background input as
Ornstein-Uhlenbeck process (best ref ?). \\

So in general :
\begin{equation}
  \label{eq:g-bg}
  \tau_{syn} \frac{d G^{bg}_{syn}}{dt} = \mu_{syn} -G^{bg}_{syn} + \sigma_{syn}
  \, \cdot \, \eta(t)
\end{equation}

where $\eta(t)$ is a gaussian white noise. 

However this input varies considerably between Up and Down states. The
Up state is characterized by a very high synaptic bombardment whereas
the synaptic activity is almost null in the down state. This can be
seen in the values of \autoref{tab:params}. The autocorrelation time
$\tau_{syn}$ is taken as the same as the synaptic decay time in
explicit models of synaptic conductance time course.


\subsection{Thalamic input evoked by stimuli}
\label{sec:ntwk-background}
 
The neuron also receive stimulus evoked input. The first one is the
thalamic input. We model this input as a sum of synaptic event. A
individual event is a jump of value $Q_{syn}$ of the conductance
followed by an exponential decay of time constant $\tau_{syn}$.

We introduce the number of active afferent thalamic connection as a
response to the stimulus : $N_{syn}^{thal}$. This can depend on the
stimulus nature and intensity.

For simplicity all events arrive at the same time.

Therefore the conductance input due to thalamic excitation as a
reponse to the stimulus will be :

\begin{equation}
  \label{eq:ex-gexc-thal}
  G_e^{thal}(t) = \sum_{i=1}^{N_e^{thal}} Q_e \, e^{-\frac{t-t_{arr}}{\tau_e}}
  \, \, H(t-t_{arr})
\end{equation}
where $H$ is the Heaviside function. The same applias to the
inhibitory conductance.

\quad \\
\quad \textbf{N.B. we could introduce shifts in synaptic arrival times
  without problems as well as other synaptic properties (non
  exponential decay).}

\subsection{Cortical input evoked by stimuli}
\label{sec:ntwk-background}

For simplicity we use the same synaptic properties as for the thalamic
synapses.

We introduce the number of active afferent cortical connection as a
response of the stimulus : $N_{syn}^{cort}$. Again, this is able to
depend on the stimulus nature and intensity.

\textbf{in the following, the Down-state response is not sensitive to
  this cortical network input as for the given stimuli no spiking is
  observed (or they're discarded) so no recurrent activity can be
  sensed.}

\subsection{Parameters}
\label{sec:params}

\begin{table}[H]
\centering
\hspace{-1cm}
\begin{tabular}{ccc}
  \multicolumn{1}{c}{name} & \multicolumn{1}{l}{symbol} &
  \multicolumn{1}{c}{value} \\[.2cm] \hline

  Input conductance & $g_L$  & 10 nS \\[.01cm]

  Leak reversal potential & $E_L$  & -65 mV \\[.01cm]

  Membrane capacitance  & $C_m$   & 200 pF \\[.01cm]

  excitatory synaptic weight  & $Q_e$   &  0.6 nS \\[.01cm]
  excitatory synaptic time deacy  & $\tau_e$   &  7 ms \\[.01cm]

  inhibitory synaptic weight  & $Q_i$   &  1 nS \\[.01cm]
  inhibitory synaptic time deacy  & $\tau_i$   &  5 ms \\[.01cm]

  Up state : mean background excitation 
  & $\mu^{up}_{e}$  & 15 nS \\[.01cm]

  Up state : std background excitation 
  & $\sigma^{up}_{e}$  & 5 nS \\[.01cm]

  Up state : mean background inhibition
  & $\mu^{up}_{i}$  & 40 nS \\[.01cm]

  Up state : std background inhibition
  & $\sigma^{up}_{i}$  & 15 nS \\[.01cm]

  Down state : mean background excitation
  & $\mu^{down}_{e}$  & 1 nS \\[.01cm]

  Down state : std background excitation
  & $\sigma^{down}_{e}$  & 0.2 nS \\[.01cm]

  Down state : mean background inhibition
  & $\mu^{down}_{i}$  & 2 nS \\[.01cm]

  Down state : std background inhibition
  & $\sigma^{down}_{i}$  & 0.5 nS \\[.01cm]

  \hline
\end{tabular}
\caption{Parameters of the models}
\label{tab:params}
\end{table}

\subsection{Calculus of PSP effects}
\label{sec:calc}

The stimulus-evoked membrane potential response is the response of the
synaptic stimulus-evoked input with synaptic stochastic-like input. \\

To approximately calculate the effect of the stimulus on the membrane
potential, we use the same approximation used in Kuhn et al. 2004
\cite{Kuhn2004}, namely that the driving force is not modified within
the time course of the reponse to the synaptic event. This approximation 
might not hold expecially for high stimulus range, therefore we 
will everytime compare it to numerical simulations. \\

Within the background input, an estimate of the mean membrane
potential is :
\begin{equation}
  \label{eq:vm-mean}
  \overline{V} = \frac{\mu_e E_e + \mu_e \, E_i +
  g_L E_L}{\mu_e + \mu_i + g_L} 
\end{equation}

We introduce:
\begin{equation}
  \label{eq:tm}
  \tau_m^{eff} = \frac{C_m}{\mu_e + \mu_i + g_L} 
\end{equation}

And we consider that synaptic driving forces are constant
$(E_{syn}-V(t) ) \sim (E_{syn}-\overline{V}) $ fixed by the mean
membrane potential, we can rewrite \autoref{eq:single-comp} as :
\begin{equation}
  \label{eq:cst-driving}
  \tau^{eff}_m \, \, \frac{dV}{dt} = \overline{V} - V +
  \frac{\tau_m^{eff}}{C_m} \, \sum_{syn \in \{e,i\}} (G_{syn}^{thal} + 
  G_{syn}^{cort}) \big(E_{syn} - \overline{V} \big)
\end{equation}

Within this approximation, the effect of different synaptic events do
not interact within each other (via the variation of the driving
force) so that they sum independently. So we calculate the effect of
one event and then linearly summate. \\

If the stimulation starts at $t_{arr}=0$, for $t \in [0, \infty], $we
get:
\begin{equation}
  \label{eq:cst-driving}
  \tau^{eff}_m \, \, \frac{dV}{dt} = \overline{V} - V +
  \frac{\tau_m^{eff}}{C_m} \, Q_{syn} \, 
  \big(E_{syn} - \overline{V} \big) e^{-\frac{t}{\tau_{syn}}}
\end{equation}

The solution with $V(0)=\overline{V}$ is:
\begin{equation}
  \label{eq:psp}
  V(t) = \overline{V} + 
  \frac{\tau_{syn} \, Q_{syn} \, \tau_m^{eff} }{C_m
    \, (\tau_m^{eff} - \tau_{syn})}
   (E_{syn} - \overline{V}) \, ( e^{-\frac{t}{\tau^{eff}_{m}}}  -
   e^{-\frac{t}{\tau_{syn}}})
\end{equation}

From this we obtain that the max amplitude time $t_{max}$ is the
solution of $- \tau_{syn} \, e^{-\frac{t_max}{\tau^{eff}_{m}}} +
\tau^{eff}_{m} e^{-\frac{t}{\tau_{syn}}} = 0 $, in practice, we will
solve this using a newton method and then we obtain the maximum
amplitude response $V(t_{max})$ (adapted if multiple synaptic
populations).

\subsection{Cortical network : excitability}
\label{sec:ntwk-excitability}

We want to have an estimate of the excitability of the cortical network
with respect to a given stimulus. \\

Let's say that we have a stimulus $\gamma_{stim}$ and that, for a
given conductance state, we know how is translated this stimulation
into the maximum depolarization : $f_{dep}(\gamma_{stim})$.

We hypothesize that neurons within the network are distributed with
respect to their membrane potential via the distribution $\rho_S(V)$,
where $S \in \{Up,Down\}$ is the distribution of membrane potential
for a given conductance state. \\

We discard spiking neurons such that $V>V_{thre}$, they participate to
the baseline firing rate hence to the baseline depolarization (for the
Up-state in particular). So now, the additional probability to trigger
a spike for a given stimulus intensity $\gamma_{stim}$ is the
probability that neurons are able to reach the threshold, i.e.:

\begin{equation}
  \label{eq:stim-effect}
  p(\gamma_{stim}) = \int^{V_{thre}}_{V_{thre}-f_{dep}(\gamma_{stim})} \rho_S(V) \, dV
\end{equation}

We simplified $\rho_S(V)$ by its gaussian approximation (see Rudolph
et al. \cite{Rudolph2004}) and we cut it above threshold: 

\begin{equation}
  \label{eq:rho-rudolph}
  \left\{
  \begin{split}
    \rho_S(V) & = \frac{\sqrt{2}}{ \sqrt{\pi} \, \sigma_V \, (
      \textnormal{Erf}(\frac{V_{thre}-\overline{V}}{\sqrt{2} \, 
        \sigma_V})+1)}
    e^{-\frac{(V-\overline{V})^2}{2 \, \sigma_V^2}} \\[.2cm]
    & \sigma_V = ...
  \end{split}
  \right.
\end{equation}

 So for $p(\gamma_{stim})$, we get:
\begin{equation}
  \label{eq:stim-effect2}
  p(\gamma_{stim}) = \frac{
    \textnormal{Erf}(\frac{V_{thre}-\overline{V}}{\sqrt{2} \, \sigma_V})
    - \textnormal{Erf}(\frac{V_{thre}-\overline{V}- 
      f_{dep}(\gamma_{stim})}{\sqrt{2} \, \sigma_V})}{
    \textnormal{Erf}(\frac{V_{thre}-\overline{V}}{\sqrt{2} \, \sigma_V})+1}
\end{equation}

ok, nice for $f(\gamma)=0$ and $f(\gamma)\rightarrow \infty$. This
gives the same qualitative output as in H\^o and Destexhe 2000
\cite{Ho2000}.


\begin{figure}[H]
  \centering
  \includegraphics[width=.4\linewidth]{figs/proba_resp} \vspace{.7cm}
  \caption{Plotting the results of \autoref{eq:stim-effect2} for the
    high (Up state) and low (Down state) conductance state with the
    parameters of \autoref{tab:params}.}
  \label{fig:gain}
\end{figure}

\subsection{Recruitment of the cortical stimulation}
\label{sec:scort-stim-recruitment}

We want an gross estimate of the recruitment of spiking neurons for
different stimulation level.\\

The stimulation is made with a dipolar electrode, so the expression of
the extracellular current can be captured by:

\begin{equation}
  \label{eq:Ir}
  I_{stim}(r) = \frac{I_{max}}{1+(\frac{r}{r_0})^2}
\end{equation}
varying the stimulus intensity make $I_{max}$ vary.\\

We hypothesize a constant neuron density per surface area (not volumic
because of the layer organisation of the cortex), this neuronal
density will be called $D_n$ . From the electrode the number of
neurons between $r$ and $r+dr$ verifies:
\begin{equation}
  \label{eq:nrn-number}
  N(r) \, dr = 2 \, \pi \, D_n \, \, dr
\end{equation}

We look for the number of neurons $N(I)$ feeling the stimulus
intensity between $I$ and $I+dI$, it verifies $N(I) \, dI = N(r) \,
dr$, so $N(I) = N(r) \, \frac{dr}{dI}$, we invert \autoref{eq:Ir}, we
get $r(I) = r_0 \, \sqrt{\frac{I_{max}}{I}-1}$, so:
\begin{equation}
  \label{eq:proba-I}
  N(I) = \frac{\pi \, D_n \, r_0^2 \, I_{max}}{2 \, \, I^2}
\end{equation}

This should apply to the 10000 neurons between $r=0$ and $r=1$mm (note
that the normalization change with respect to $I_{max}$). To
illustrate the distribution that we obtain:


\begin{figure}[H]
  \centering
  \includegraphics[width=.6\linewidth]{figs/stim_density}
  \caption{Stimulus histogram resulting from the ohmic dissipation of
    the stimulation. Indeed within the interesting area, neurons feel
    different values of the stimulus.}
  \label{fig:stim-hist}
\end{figure}

We grossly translate this into a membrane potential stimulation, 

\begin{figure}[H]
  \centering
  \includegraphics[width=.6\linewidth]{figs/stim_density_v}
  \caption{Stimulus histogram in terms of depolarization for 2
    different levels of stimulation (zoom over the interseting
    range).}
  \label{fig:stim-hist-v}
\end{figure}

Now we merge this with the \autoref{fig:gain}, to calculate the number
of active neurons with respect to the conductance state.
\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{figs/recruit}
  \caption{Method allowing to calculate the number of active neurons
    in the Up and Down states respectively.}
  \label{fig:recruiting-method}
\end{figure}

From this number of neurons, we take 20\% of them being inhibitory and
80\% of them being excitatory. We also take that 5\% of them are
connected to the recorded cell.

So now, we can associate a stimulus value to number of recruited
neurons and finally to the maximum PSP value thanks to the calculus of
the previous section.

\begin{figure}[H]
  \centering
  \includegraphics[width=.5\linewidth]{figs/recruited_number}
  \caption{Total number of spiking cells with respect to the stimulus
    intensity, a fraction of them will actually stimulate the recorded
    cell.}
  \label{fig:recruited-num}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=.6\linewidth]{figs/sim_cortical}
  \caption{Estimated PSP from the previous number of spiking cells
    presented in \autoref{fig:recruited-num} and with a connectivity of
    5\% and 20\% of inhibitory cells.}
  \label{fig:sim-cortical}
\end{figure}

\end{document}

