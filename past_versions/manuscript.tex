\documentclass[11pt,a4paper]{article}

\usepackage{graphicx}
%\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[textwidth=18cm,columnsep=1cm,bottom=1.4cm,top=1.5cm]{geometry}
\usepackage[colorlinks=true]{hyperref}
\hypersetup{allcolors = blue} % to have all the hyperlinks in 1 color

\title{ \vspace{-1cm} 
\textsc{\small Some modeling about} \\[.2cm] 
\textbf{Gain modulation and functional scaling
      of synaptic potentials during up states 
      in auditory cortex} \\[.01cm]
  \textnormal{R. Reig, R.Vergara \& M.V. Sanchez Vives} }

\author{Yann Zerlaut \& Alain Destexhe}

\date{\today}

\begin{document}
\maketitle

\begin{figure}[b!]
  \centering
  \includegraphics[width=\linewidth]{figs/fig.png}
  \caption{Experimental data. Post synaptic amplitude of the membrane
    potential response to auditory (\textbf{A}), thalamic (\textbf{B})
    and cortical (\textbf{C}) stimulation as a function of the
    stimulus intensity. }
  \label{fig:exp}
\end{figure}

% \section*{Question/Comments on the manuscript}
% \label{sec:comments}

% \begin{itemize}
% \item \textit{"To confirm that the PSPs were excitatory, their
%     amplitude was often examined at different membrane potential, Page
%     4."} Does that mean that the stimulation recruits purely
%   excitatory neurons ? The model doesn't, the response is made of the
%   sum of excitatory and inhibitory synaptic transmission.
% \item In the case of the thalamic and auditory stimulation, where do
%   the PSPs come from, they are only due to the recurrence of the
%   cortical network ? There are no trace of the thalamic stimulation ?
%   Why,  because it is statistically unlikely to get a cell that has a
%   thalamic afferent that has been activated ?
% \end{itemize}


\section{Methods}
\label{sec:model}

\subsection{Cell model}
\label{sec:cell-model}

We model the neuron as a single compartment, i.e. we neglect the
spatial extent of the neurons and we consider that the whole
computation can be described by a single value: the membrane potential
of the soma. We adapt synaptic parameters to reproduce somatic
response. \\

We are interested in the subthreshold response of the neuron, so the
membrane will only exhibit 2 features: passive currents and synaptic
currents.

The passive properties are described by a simple RC circuit, the
capacitive current is characterized by a membrane capacitance $C_m$
the leak current is set by a conductance $g_L$ and a reversal
potential $E_L$.

\begin{equation}
  \label{eq:single-comp}
  C_m \frac{dV}{dt} = g_L \, (E_L - V) + I^{syn}(V,t)
\end{equation}

The synaptic currents $I^{syn}(V,t)$ is made of excitatory and
inhibitory input with reversal potentials $E_e$ and $E_i$. Their
respective conductances $G_e$ and $G_i$ will be determined by the sum
of the background and stimulus evoked activity.

\begin{equation}
  \label{eq:syn-current}
  I^{syn}(V,t) = G_e(t) \, (E_e - V) + G_i(t) \, (E_i - V)
\end{equation}


\subsection{Cortical input : background activity}
\label{sec:ntwk-background}

The cortical network sends excitatory and inhibitory input to the
recorded cell, we will model this background input as
Ornstein-Uhlenbeck process (best ref ??). \\

So in general :
\begin{equation}
  \label{eq:g-bg}
  \tau_{syn} \frac{d G^{bg}_{syn}}{dt} = \mu_{syn} -G^{bg}_{syn} + \sigma_{syn}
  \, \cdot \, \eta(t)
\end{equation}

where $\eta(t)$ is a gaussian white noise. 

However this input varies considerably between Up and Down states. The
Up state is characterized by a very high synaptic bombardment whereas
the synaptic activity is almost null in the down state. This can be
seen in the values of \ref{tab:params}. The autocorrelation time
$\tau_{syn}$ is taken as the same as the synaptic decay time in
explicit models of synaptic conductance time course.


\subsection{Input evoked by the cortical stimulus}
\label{sec:ntwk-background}
 
In the case of the cortical bipolar stimulation, the neuron also
receive an additional input from the cortical network\footnote{we
  neglect the possibility of a cortico-thalamic followed by a
  thalamo-cortical reponse }. We model this input as a sum of
excitatory and inhibitory events. A individual event is a jump of value
$Q_{syn}$ of the conductance followed by an exponential decay of time
constant $\tau_{syn}$.

We introduce the number of active afferent cortical connection as a
response to the stimulus : $N_{syn}^{cort}$. This will depend on the
intensity of the stimulus.

For simplicity all events arrive at the same time.

Therefore the conductance input as a reponse to the cortical stimulus
will be :

% \begin{equation}
%   \label{eq:ex-g-cort}
%   \left\{
%   \begin{split}
%   G_e^{cort}(t) & = \sum_{i=1}^{N_e^{cort}} Q_e \, e^{-\frac{t-t_{arr}}{\tau_e}}
%   \, \, H(t-t_{arr}) \\
%   G_i^{cort}(t) & = \sum_{i=1}^{N_i^{cort}} Q_i \, e^{-\frac{t-t_{arr}}{\tau_i}}
%   \, \, H(t-t_{arr})
%   \end{split}
%   \right.
% \end{equation}
where $H$ is the Heaviside function.\footnote{N.B. we could introduce
  shifts in synaptic arrival times without problems as well as other
  synaptic properties (non exponential decay).}

\begin{table}[t!]
\centering
\hspace{-1cm}
\begin{tabular}{ccc}
  \multicolumn{1}{c}{name} & \multicolumn{1}{l}{symbol} &
  \multicolumn{1}{c}{value} \\[.2cm] \hline

  Input conductance & $g_L$  & 10 nS \\[.01cm]

  Leak reversal potential & $E_L$  & -65 mV \\[.01cm]

  Membrane capacitance  & $C_m$   & 200 pF \\[.01cm]

  excitatory synaptic weight  & $Q_e$   &  0.6 nS \\[.01cm]
  excitatory synaptic time deacy  & $\tau_e$   &  7 ms \\[.01cm]

  inhibitory synaptic weight  & $Q_i$   &  1 nS \\[.01cm]
  inhibitory synaptic time deacy  & $\tau_i$   &  5 ms \\[.01cm]

  Up state : mean background excitation 
  & $\mu^{up}_{e}$  & 15 nS \\[.01cm]

  Up state : std background excitation 
  & $\sigma^{up}_{e}$  & 5 nS \\[.01cm]

  Up state : mean background inhibition
  & $\mu^{up}_{i}$  & 40 nS \\[.01cm]

  Up state : std background inhibition
  & $\sigma^{up}_{i}$  & 15 nS \\[.01cm]

  Down state : mean background excitation
  & $\mu^{down}_{e}$  & 1 nS \\[.01cm]

  Down state : std background excitation
  & $\sigma^{down}_{e}$  & 0.2 nS \\[.01cm]

  Down state : mean background inhibition
  & $\mu^{down}_{i}$  & 2 nS \\[.01cm]

  Down state : std background inhibition
  & $\sigma^{down}_{i}$  & 0.5 nS \\[.01cm]

  Threshold for spike triggering
  & $V_{thre}$  & -50 mV \\[.01cm]

  Network connectivity
  & $\epsilon$  & 5\% \\[.01cm]

  Network ratio of inh cells
  & $g$  & 25\% \\[.01cm]

  \hline
\end{tabular}
\caption{Parameters of the models}
\label{tab:params}
\end{table}

\subsection{Calculus of PSP effects}
\label{sec:calc}

The stimulus-evoked membrane potential response is the response of the
synaptic stimulus-evoked input with synaptic stochastic-like input. \\

To approximately calculate the effect of the stimulus on the membrane
potential, we use the same approximation used in Kuhn et al. 2004
\cite{Kuhn2004}, namely that the driving force is not modified within
the time course of the reponse to the synaptic event. This approximation 
might not hold expecially for high stimulus range, therefore we 
will everytime compare it to numerical simulations. \\

Within the background input, an estimate of the mean membrane
potential is :
\begin{equation}
  \label{eq:vm-mean}
  \overline{V} = \frac{\mu_e E_e + \mu_e \, E_i +
  g_L E_L}{\mu_e + \mu_i + g_L} 
\end{equation}

We introduce:
\begin{equation}
  \label{eq:tm}
  \tau_m^{eff} = \frac{C_m}{\mu_e + \mu_i + g_L} 
\end{equation}

And we consider that synaptic driving forces are constant
$(E_{syn}-V(t) ) \sim (E_{syn}-\overline{V}) $ fixed by the mean
membrane potential, we can rewrite \ref{eq:single-comp} as :
\begin{equation}
  \label{eq:cst-driving}
  \tau^{eff}_m \, \, \frac{dV}{dt} = \overline{V} - V +
  \frac{\tau_m^{eff}}{C_m} \, \sum_{syn \in \{e,i\}} (G_{syn}^{thal} + 
  G_{syn}^{cort}) (E_{syn} - \overline{V} )
\end{equation}

Within this approximation, the effect of different synaptic events do
not interact within each other (via the variation of the driving
force) so that they sum independently. So we calculate the effect of
one event and then linearly summate. \\

If the stimulation starts at $t_{arr}=0$, for $t \in [0, \infty], $we
get:
\begin{equation}
  \label{eq:cst-driving}
  \tau^{eff}_m \, \, \frac{dV}{dt} = \overline{V} - V +
  \frac{\tau_m^{eff}}{C_m} \, Q_{syn} \, 
  (E_{syn} - \overline{V} ) e^{-\frac{t}{\tau_{syn}}}
\end{equation}

The solution with $V(0)=\overline{V}$ is:
\begin{equation}
  \label{eq:psp}
  V(t) = \overline{V} + 
  \frac{\tau_{syn} \, Q_{syn} \, \tau_m^{eff} }{C_m
    \, (\tau_m^{eff} - \tau_{syn})}
   (E_{syn} - \overline{V}) \, ( e^{-\frac{t}{\tau^{eff}_{m}}}  -
   e^{-\frac{t}{\tau_{syn}}})
\end{equation}

We write $A_{syn} = \frac{\tau_{syn} \, Q_{syn} \, \tau_m^{eff} }{C_m
  \, (\tau_m^{eff} - \tau_{syn})} (E_{syn} - \overline{V})$. Now in
the case of the recorded neuron stimulated by $N_e$ excitatory neurons
and $N_i$ inhibitory neurons in total, the total membrane potential
response is of the form:
\begin{equation}
  \label{eq:psp-final}
  V(t) = \overline{V} + N_e \, A_e \, ( e^{-\frac{t}{\tau^{eff}_{m}}}  -
  e^{-\frac{t}{\tau_{e}}})
  + N_i \, A_i \, ( e^{-\frac{t}{\tau^{eff}_{m}}}  -
  e^{-\frac{t}{\tau_{i}}})
\end{equation}

From this we obtain that the max amplitude time $t_{max}$ is the
solution of:
\begin{equation}
  \label{eq:tmax}
  -(\tau^{eff}_{m})^{-1} \, (N_e \, A_e + N_i \, A_i)
  \, e^{-\frac{t_{max}}{\tau^{eff}_{m}}} +
  \tau_{e}^{-1} \, N_e \, A_e \, e^{-\frac{t_{max}}{\tau^{e}}} +
  \tau_{i}^{-1} \, N_i \, A_i \, e^{-\frac{t_{max}}{\tau^{i}}} = 0  
\end{equation}
in practice, we will solve this using a Newton method and then we
compute the maximum amplitude response by evaluating $V(t_{max})$.

\subsection{Cortical network : excitability}
\label{sec:ntwk-excitability}

We want to have an estimate of the excitability of the cortical network
with respect to a given stimulus. \\

Let's say that we have a stimulus $\gamma_{stim}$ and that, for a
given conductance state, we know how is translated this stimulation
into the maximum depolarization : $f_{dep}(\gamma_{stim})$.

We hypothesize that neurons within the network are distributed with
respect to their membrane potential via the distribution $\rho_S(V)$,
where $S \in \{Up,Down\}$ is the distribution of membrane potential
for a given conductance state. \\

We discard spiking neurons such that $V>V_{thre}$, they participate to
the baseline firing rate hence to the baseline depolarization (for the
Up-state in particular). So now, the additional probability to trigger
a spike for a given stimulus intensity $\gamma_{stim}$ is the
probability that neurons are able to reach the threshold, i.e.:

\begin{equation}
  \label{eq:stim-effect}
  p(\gamma_{stim}) = \int^{V_{thre}}_{V_{thre}-f_{dep}(\gamma_{stim})} \rho_S(V) \, dV
\end{equation}

We simplified $\rho_S(V)$ by its gaussian approximation (see Rudolph
et al. \cite{Rudolph2004}) and we cut it above threshold: 

\begin{equation}
  \label{eq:rho-rudolph}
  \left\{
  % \begin{split}
  %   \rho_S(V) & = \frac{\sqrt{2}}{ \sqrt{\pi} \, \sigma_V \, (
  %     \textnormal{Erf}(\frac{V_{thre}-\overline{V}}{\sqrt{2} \, 
  %       \sigma_V})+1)}
  %   e^{-\frac{(V-\overline{V})^2}{2 \, \sigma_V^2}} \\[.2cm]
  %   & \sigma_V = ...
  % \end{split}
  \right.
\end{equation}

 So for $p(\gamma_{stim})$, we get:
\begin{equation}
  \label{eq:stim-effect2}
  p(\gamma_{stim}) = \frac{
    \textnormal{Erf}(\frac{V_{thre}-\overline{V}}{\sqrt{2} \, \sigma_V})
    - \textnormal{Erf}(\frac{V_{thre}-\overline{V}- 
      f_{dep}(\gamma_{stim})}{\sqrt{2} \, \sigma_V})}{
    \textnormal{Erf}(\frac{V_{thre}-\overline{V}}{\sqrt{2} \, \sigma_V})+1}
\end{equation}

ok, nice for $f(\gamma)=0$ and $f(\gamma)\rightarrow \infty$. This
gives the same qualitative output as in H\^o and Destexhe 2000
\cite{Ho2000}.


\subsection{Recruitment of the cortical stimulation}
\label{sec:scort-stim-recruitment}

We want an gross estimate of the recruitment of spiking neurons for
different stimulation level.\\

The stimulation is made with a dipolar electrode, so the expression of
the extracellular current can be captured by:

\begin{equation}
  \label{eq:Ir}
  I_{stim}(r) = \frac{I_{max}}{1+(\frac{r}{r_0})^2}
\end{equation}
varying the stimulus intensity make $I_{max}$ vary.\\

We hypothesize a constant neuron density per surface area (not volumic
because of the layer organisation of the cortex), this neuronal
density will be called $D_n$ . From the electrode the number of
neurons between $r$ and $r+dr$ verifies:
\begin{equation}
  \label{eq:nrn-number}
  N(r) \, dr = 2 \, \pi \, D_n \, \, dr
\end{equation}

We look for the number of neurons $N(I)$ feeling the stimulus
intensity between $I$ and $I+dI$, it verifies $N(I) \, dI = N(r) \,
dr$, so $N(I) = N(r) \, \frac{dr}{dI}$, we invert \ref{eq:Ir}, we
get $r(I) = r_0 \, \sqrt{\frac{I_{max}}{I}-1}$, so:
\begin{equation}
  \label{eq:proba-I}
  N(I) = \frac{\pi \, D_n \, r_0^2 \, I_{max}}{2 \, \, I^2}
\end{equation}

This should apply to the 10000 neurons between $r=0$ and $r=1$mm (note
that the normalization change with respect to $I_{max}$). To
illustrate the distribution that we obtain:

\subsection{Stimulation effect on neurons}
\label{sec:stim-effect}

We arbitrary set the rule to translate the extracellular stimulus
value into a maximum depolarization to:
\begin{equation}
  \label{eq:stim-rule}
  \Delta V (I) = f_{dep}(I) = \Gamma \cdot I \qquad \textnormal{with : }
  \Gamma = 8/3 \, \, \frac{mV}{\mu A}
\end{equation}
This rule is inferred from the fact that at the minimal stimulatory
level, a response is observed in the Down state, so the minimal
stimulation level should at least excite one neuron in the Down state.

\section{Results}

\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{fig1.pdf}
  \caption{Network excitability and cell properties differences during
    Up and Down states explain the gain modulation of the post
    synaptic potentials as a response to a stimulation within the
    cortical network. \textbf{A} the bipolar stimulation creates a
    space-dependent extracellular current, \textbf{B} this results in
    an heterogenous stimulation $N(I)$ (\ref{eq:proba-I}) within
    the local cortical network given the decaying stimulus intensity
    $I(r)$ and the increasing cell density $N(r)$. \textbf{C} The
    activation function represents the excitability of the local
    cortical network, calculated from \ref{eq:stim-effect2} with
    the parameters of \ref{tab:params} . \textbf{D} \& \textbf{E}
    The number of responsive neurons to the stimulation depends on the
    activation function and of the stimulation level, example is shown
    for $I_0=80 \mu$A for Up and Down states respectively, the
    activation curves have been adapted to the stimulus thanks to the
    rule \ref{eq:stim-rule}. \textbf{F} For each stimulation level
    we can then calculate the number of neurons activated by the
    stimulation. \textbf{G} For given network parameters
    ($\epsilon=5\%$ and $g=0.25$), we can estimate the network input
    to the recorded cell and deduce the maximum post-synaptic
    potential from \ref{eq:psp-final} and \ref{eq:tmax} , this
    should be compared to the experimental results of
    \ref{fig:exp}\textbf{C} (cortical stimulation). \newline
    \textit{N.B. the scales can be adjusted as there the freedom of
      \ref{eq:stim-rule}}}
  \label{fig:cort}
\end{figure*}


\subsection{Gain modulation as a response to 
  cortical stimulations}

We illustrate on \ref{fig:cort} the full workflow describing the
cortical stimulation response. \\

The bipolar stimulation in \ref{fig:cort}\textbf{A} spreads over
the local cortical network of interest (the $\sim$1mm circonference
around the stimulation electrode where the recorded cell also lies).

Each neuron within this local network sees a stimulation that depends
on his distance with respect to the electrode, the extracellular
current decays as stated by \ref{eq:Ir}, the number of neurons
raises with distance from the electrode (\ref{eq:nrn-number}).

We are then able to calculate the distribution of the stimulation felt
by the neurons of the local cortical network. We plot it in
\ref{fig:cort}\textbf{B} for two different level of injected
current $I_0$. Note that the boundaries of the histogram are different
in the two cases, the stimulation current within the network delimited
by $r_{max}$ has values between $I_0$ (the maximum value) and
$I(r_{max})=\frac{I_0}{1+(\frac{r_{max}}{r_0})^2}$ (the minimum
value). 

Each stimulation value is translated into a depolarization of the
neuron according to the rule \ref{eq:stim-rule}. \\

Then, the cortical network will translate this stimulation into
spiking very differently for the two different network state. In the
Up state, the background noise will amplify the stimulation, indeed
the depolarization summed with the membrane potential fluctuations can
bring many neurons to supra-threshold levels and evoke spiking. This
is not the case in the Down state, where whether or not spikes are
elicited is an \textit{all-or-none} process, all the neurons that are
depolarized above threshold fires, the others can not. Following H\^o
and Destexhe 2000, we introduce the \textit{activation function} that
translate the stimulus value into a probability to evoke a spike (in
addition to the background spiking, neurons participating to the
baseline firing rate are discarded). This can be calculated
explicitely from the fluctuations of the membrane potential and a
basic threshold mechanism for spiking, this leads to
\ref{eq:stim-effect2}, it is illustrated in
\ref{fig:cort}\textbf{C} for the Up and Down states respectively.

By using the histogram over neurons of the stimulus value seen by the
neurons, we can now apply the activation function on this distribution
and calculate what is the number of neurons activated. An example for
a given stimulus value is presented in \ref{fig:cort} \textbf{D}
\& \textbf{E}, the shaded parts of the histogram represents the
proportion of activated neurons.

We do this for every stimulation level and we count the total number
of activated neurons within the cortical network. This leads to the
plot of \ref{fig:cort}\textbf{F}.

Thus, for every stimulation level we have a number of activated
neurons, we split them into excitatory and inhibitory
neurons\footnote{hypothesizing that there is no excitability
  difference between excitatory and inhibitory neurons} according to
the ratio $g=25\%$ of inhibitory cells. Then we can calculate what is
the number of afferent activated neurons ($N_e$ and $N_i$) onto the
recorded cell given a mean connectivity $\epsilon=5\%$. The maximum
depolarization value induced by this stimulation is then given by
calculating the time of maximum amplitude from \ref{eq:tmax} and
plugging it into \ref{eq:psp-final}. This results in
\ref{fig:cort}\textbf{G}

\quad \\


\textit{The cortical stimulation seems to be the most simple and the
  less variable phenomena, it could make sense to start the study with
  this. The single neuron response corresponds to the neurons-averaged
  response, so the phenomena show less heterogeneity/variability. The
  model works well, it can be explained with an undifferenciated
  recruitment of excitation and inhibition within the cortical
  network. This does not seem to be the case anymore for the thalamic
  and auditory stimulation, more complex recruitment seems to happen.}



\subsection{Gain modulation as a response to 
  thalamic stimulations}

In this case, it seems that the recruitement of the thalamic
stimulation within the cortical network is not random anymore but show
some bias. We test the authors hypothesis, namely that thalamic cells
preferentially target the inhibitory cells. This is presented in
\ref{fig:thal}. \\

\begin{figure}[t]
  \centering
  \begin{minipage}[b]{0.4\linewidth}\centering
  \centering
  \includegraphics[width=\linewidth]{figs/thal_exc_inh} \vspace{.7cm}
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}\centering
  \includegraphics[width=\linewidth]{figs/more_g_effect}
  \end{minipage}
  \caption{Thalamic stimulation. Implementing the hypothesis of a
    non-homogeneous recruitment of the thalamic stimulation within the
    cortical network. Two cases are compared, with dashed line (like
    \ref{fig:cort}) the case where the thalamic input would
    randomly sample the cortical network and then have $g=25\%$ chance
    of targetting an inhibitory cell. Whereas in the second case,
    plain line, there is a biased recruitment that bring the chance of
    targeting an inhibitory cell to $g=50\%$. This should reproduce
    \ref{fig:exp}\textbf{B} (thalamic stimulation).}
  \label{fig:thal}
\end{figure}

Note that we do not have modeled explicitely the recruitment of the
stimulation (how do the stimulus depolarizes neurons and what is their
distribution for different stimulus value). This could have an impact
on the function shown in \ref{fig:thal}. We just took the same
activation function of \ref{fig:cort}\textbf{F} and changed the
ratio of inhibitory to excitatory cells $g$.

\subsection{Gain modulation as a response to 
  auditory stimulations}

To be done... \\

\textit{But very interesting phenomena : variability within the
  response.  There is a qualitative difference between the individual
  cell responses and the cell averaged response. This could be
  explicitely modeled, but we would need some more details about the
  variations around the mean response.}


% \newpage
% \section{PSP function study}
% \label{sec:results}

% \subsection{Case 1 : purely excitatory thalamic 
%   afference on the cell}

% \begin{figure}[H]
%   \label{fig:case1}
% \end{figure}

% In this case the network is only a background noise generator, it does
% not participate actively to the cell's response. Only the thalamic
% input is determined by the stimulus 

% \begin{figure}[H]
%   \centering
%   \begin{minipage}[b]{0.4\linewidth}\centering
%   \centering
%   \includegraphics[width=\linewidth]{figs/pure_thal} \vspace{.7cm}
%   \end{minipage}
%   \begin{minipage}[b]{0.5\linewidth}\centering
%   \includegraphics[width=\linewidth]{figs/pure_thal_model}
%   \end{minipage}
%   \caption{Schematic representation and model output: purely
%     excitatory thalamic afference on the cell}
%   \label{fig:case1}
% \end{figure}

% The two curves are monotonic and can not cross. The large difference
% between the two different input impedance explains the shift between
% the two curve\footnote{it is likely that releasing the "constant
%   driving force" approximation would put the curve closer as the
%   \textit{high conductance} state would be poorly affected because of
%   its low polarization whereas the \textit{low conductance} state has
%   a high polarization so a big change in the driving force and it also
%   start further from the reversal potential so the absolute calue of
%   this change will be important.}.


% \subsection{Case 2 : purely excitatory thalamic 
%   and cortical afference on the cell}

% \begin{figure}[H]
%   \centering
%   \begin{minipage}[b]{0.4\linewidth}\centering
%   \centering
%   \includegraphics[width=\linewidth]{figs/exc_thal_cort} \vspace{.7cm}
%   \end{minipage}
%   \begin{minipage}[b]{0.5\linewidth}\centering
%   \includegraphics[width=\linewidth]{figs/exc_cort_thal_model}
%   \end{minipage}
%   \caption{Schematic representation and model output: purely
%     excitatory thalamic and cortical afference on the cell. In this
%     model, the excitation of the recurrent cortical network is
%     proportional to the stimulus.}
%   \label{fig:case2}
% \end{figure}

% In this case the cortical network is also excited by the thalamic
% network, this interaction is purely excitatory so the cortical
% activity can only raise. In this configuration the cortical network
% can only excite the cell, so the value $G_e^{cort}(t)$ is controlled
% by the recruitment that the thalamic stimulation does on the cortical
% network, this recruitment is represented by the parameter
% $N_e^{cort}$, the number of cortical synapses that stimulate the
% neuron after the stimulation.


% \textbf{N.B. in this model, the excitatory PSP should show the time
%   lag of the network recruitment in the Up state, so we expect a
%   larger rise time for the Up state than for the Down state, finding
%   this in the data would justify the approach.}

% \subsection{Case 3 : purely excitatory thalamic and
%   cortical afference on the cell with renormalization}

% \begin{figure}[H]
%   \centering
%   \begin{minipage}[b]{0.4\linewidth}\centering
%   \centering
%   \includegraphics[width=\linewidth]{figs/renorm_cort_thal} \vspace{.7cm}
%   \end{minipage}
%   \begin{minipage}[b]{0.5\linewidth}\centering
%   \includegraphics[width=\linewidth]{figs/renorm_cort_thal_model}
%   \end{minipage}
%   \caption{Schematic representation and model output: purely
%     excitatory thalamic and cortical afference on the cell with
%     renormalization of the input. In this model, the excitation of the
%     recurrent cortical network is no more proportional to the stimulus
%     but simply constant because of a renormalization hapenning in the
%     thalamo-cortical network.}
%   \label{fig:case2}
% \end{figure}

% Again, the value $G_e^{cort}(t)$ is controlled by the recruitment that
% the thalamic stimulation does on the cortical network, this
% recruitment is represented by the parameter $N_e^{cort}$ and it is
% constant whatever the stimulation level is.

% \textbf{This is a very simple mechanism that could reproduce the data
%   in the case of the auditory and cortical stimulation. As the slope
%   of the Up state curve is much lower (in response to a thalamic
%   input), by force starting from above creates a crossing of the
%   curves. Interesting to note that this does not work in the thalamic
%   stimulation.}



\footnotesize
\bibliographystyle{plain}
\bibliography{/home/yann/work/miscellaneous/refs/neuro_database}

\end{document}

